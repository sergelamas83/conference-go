# Select the base image that is best for our application
FROM python:3

# Install any operating system dependencies (if needed)

# Set the working directory to copy files into
WORKDIR /app

# Copy application code and dependencies file
COPY accounts accounts
COPY attendees attendees
COPY common common
COPY conference_go conference_go
COPY events events
COPY presentations presentations
COPY requirements.txt requirements.txt
COPY manage.py manage.py

# Upgrade pip
RUN pip install --upgrade pip

# Install Python dependencies
RUN pip install -r requirements.txt
RUN pip install requests

# Set the command to run the application
CMD gunicorn --bind 0.0.0.0:8000 conference_go.wsgi
