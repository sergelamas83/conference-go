#attendees/api_views.py
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Attendee
from events.models import Conference
from common.json import ModelEncoder
import json


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference_id=conference_id)
        response = [
            {
                'email': attendee.email,
                'name': attendee.name,
                'href': attendee.get_api_url(),
            }
            for attendee in attendees
        ]
        return JsonResponse({'attendees': response}, safe=False)

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            {
                'email': attendee.email,
                'name': attendee.name,
                'href': attendee.get_api_url(),
            },
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)
    except Attendee.DoesNotExist:
        return JsonResponse(
            {"message": "Attendee not found"},
            status=404,
        )

    if request.method == "GET":
        data = {
            'email': attendee.email,
            'name': attendee.name,
            'company_name': attendee.company_name,
            'created': attendee.created.isoformat(),
            'conference': {
                'name': attendee.conference.name,
                'href': attendee.conference.get_api_url(),
            }
        }
        return JsonResponse(data, safe=False)

    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if 'conference' in content:
                conference = Conference.objects.get(id=content['conference'])
                content['conference'] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        for field, value in content.items():
            setattr(attendee, field, value)

        attendee.save()
        return JsonResponse(
            {
                'email': attendee.email,
                'name': attendee.name,
                'company_name': attendee.company_name,
                'created': attendee.created.isoformat(),
                'conference': {
                    'name': attendee.conference.name,
                    'href': attendee.conference.get_api_url(),
                }
            },
            safe=False,
        )

    elif request.method == "DELETE":
        attendee.delete()
        return JsonResponse({"message": "Attendee deleted"})
