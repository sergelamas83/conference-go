# Generated by Django 4.0.3 on 2023-11-28 00:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attendee',
            name='company_name',
            field=models.CharField(default='', max_length=200),
        ),
    ]
