# common/json.py
from json import JSONEncoder
from events.models import Location, State
from datetime import datetime

# Create a custom JSON encoder class that inherits from JSONEncoder
class ModelEncoder(JSONEncoder):
    # Define a method to handle object serialization
    def default(self, obj):
        # Check if the object is an instance of a custom model class
        if isinstance(obj, Model):
            # Check if the object is an instance of the Location class
            if isinstance(obj, Location):
                # Create a dictionary with selected attributes from the Location object
                data = {
                    'name': obj.name,
                    'city': obj.city,
                    'room_count': obj.room_count,
                    'created': obj.created,
                    'updated': obj.updated,
                    'state': obj.state.name if obj.state else None,
                    'picture_url': obj.picture_url,
                }
                # Return the dictionary for serialization
                return data
            # Check if the object is an instance of the State class
            elif isinstance(obj, State):
                # Create a dictionary with selected attributes from the State object
                data = {
                    'name': obj.name,
                    'abbreviation': obj.abbreviation,
                }
                # Return the dictionary for serialization
                return data
            # If the object is neither Location nor State, use the default serialization method
            return super().default(obj)
        # Check if the object is an instance of the datetime class
        elif isinstance(obj, datetime):
            # Serialize the datetime object to ISO format
            return obj.isoformat()
        # If none of the above conditions are met, use the default serialization method
        return super().default(obj)
