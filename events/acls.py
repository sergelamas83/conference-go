import requests
from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    query = f"{city} {state}"
    url = f"https://api.pexels.com/v1/search?query={query}&per_page=1"

    response = requests.get(url, headers=headers)

    if response.status_code == 200:
        data = response.json()
        if data.get("photos"):
            picture_url = data["photos"][0]["url"]
            return {"picture_url": picture_url}
    
    return {"picture_url": None}

def get_weather_data(city, state):
    geo_url = f"http://api.openweathermap.org/data/2.5/weather?q={city},{state}&appid={OPEN_WEATHER_API_KEY}"
    
    response = requests.get(geo_url)
    
    if response.status_code == 200:
        data = response.json()
        lat = data["coord"]["lat"]
        lon = data["coord"]["lon"]
        
        weather_url = f"http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
        response = requests.get(weather_url)
        
        if response.status_code == 200:
            data = response.json()
            weather_data = {
                "temp": data["main"]["temp"],
                "description": data["weather"][0]["description"]
            }
            return {"weather": weather_data}
    
    return {"weather": None}
