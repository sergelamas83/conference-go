from django.urls import path
from . import api_views

urlpatterns = [
    path("conferences/", api_views.api_list_conferences, name="api_list_conferences"),
    path("conferences/<int:pk>/", api_views.api_show_conference, name="api_show_conference"),
    path("locations/", api_views.api_list_locations, name="api_list_locations"),
    path("locations/<int:id>/", api_views.api_show_location, name="api_show_location"),  # Add this line
]
