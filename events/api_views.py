from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
from .models import Location, State, Conference
from .acls import get_photo, get_weather_data
from django.utils.dateparse import parse_datetime
from common.json import ModelEncoder
import json

@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        data = [
            {
                "id": location.id,
                "name": location.name,
                "city": location.city,
                "room_count": location.room_count,
                "created": location.created.isoformat(),
                "updated": location.updated.isoformat(),
                "state": location.state.name if location.state else None,
                "picture_url": location.picture_url,
            }
            for location in locations
        ]
        return JsonResponse(data, encoder=ModelEncoder, safe=False)
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        location = Location.objects.create(**content)
        picture_url = get_photo(location.city, location.state.abbreviation)
        location.picture_url = picture_url["picture_url"]
        location.save()
        location_data = {
            "id": location.id,
            "name": location.name,
            "city": location.city,
            "room_count": location.room_count,
            "created": location.created.isoformat(),
            "updated": location.updated.isoformat(),
            "state": location.state.name if location.state else None,
            "picture_url": location.picture_url,
        }
        return JsonResponse(
            location_data,
            encoder=ModelEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    conference = get_object_or_404(Conference, id=id)

    if request.method == "GET":
        location = conference.location
        weather_data = get_weather_data(location.city, location.state.abbreviation)
        data = {
            "id": conference.id,
            "name": conference.name,
            "starts": conference.starts.isoformat(),
            "ends": conference.ends.isoformat(),
            "description": conference.description,
            "created": conference.created.isoformat(),
            "updated": conference.updated.isoformat(),
            "max_presentations": conference.max_presentations,
            "max_attendees": conference.max_attendees,
            "location": {
                "id": location.id,
                "name": location.name,
                "href": location.get_api_url(),
            },
            "weather": weather_data,
        }
        return JsonResponse(data, encoder=ModelEncoder, safe=False)

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        data = []
        for conference in conferences:
            conference_data = {
                "id": conference.id,
                "name": conference.name,
                "starts": conference.starts.isoformat() if conference.starts else None,
                "ends": conference.ends.isoformat() if conference.ends else None,
                "description": conference.description,
                "created": conference.created.isoformat() if conference.created else None,
                "updated": conference.updated.isoformat() if conference.updated else None,
                "max_presentations": conference.max_presentations,
                "max_attendees": conference.max_attendees,
                "location": conference.location.id if conference.location else None,
            }
            data.append(conference_data)
        return JsonResponse(data, encoder=ModelEncoder, safe=False)
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location

            content["starts"] = parse_datetime(content["starts"])
            content["ends"] = parse_datetime(content["ends"])
            content["created"] = parse_datetime(content["created"])
            content["updated"] = parse_datetime(content["updated"])
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            {
                "id": conference.id,
                "name": conference.name,
                "starts": conference.starts.isoformat() if conference.starts else None,
                "ends": conference.ends.isoformat() if conference.ends else None,
                "description": conference.description,
                "created": conference.created.isoformat() if conference.created else None,
                "updated": conference.updated.isoformat() if conference.updated else None,
                "max_presentations": conference.max_presentations,
                "max_attendees": conference.max_attendees,
                "location": conference.location.id if conference.location else None,
            },
            encoder=ModelEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, id):
    location = get_object_or_404(Location, id=id)

    data = {
        "id": location.id,
        "name": location.name,
        "city": location.city,
        "room_count": location.room_count,
        "created": location.created.isoformat(),
        "updated": location.updated.isoformat(),
        "state": location.state.name if location.state else None,
        "picture_url": location.picture_url,
        "location": {
                "id": location.id,
                "name": location.name,
                "href": location.get_api_url(),
            },
    }
    return JsonResponse(data, encoder=ModelEncoder, safe=False)
