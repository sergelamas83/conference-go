
from django.db import models
from django.urls import reverse

class State(models.Model):
    name = models.CharField(max_length=200)
    abbreviation = models.CharField(max_length=10)

    def __str__(self):
        return self.name

class Location(models.Model):
    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    room_count = models.PositiveSmallIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    state = models.ForeignKey(State, related_name="locations", on_delete=models.PROTECT)
    picture_url = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("api_show_location", kwargs={"id": self.pk})

class Conference(models.Model):
    name = models.CharField(max_length=200)
    starts = models.DateTimeField()
    ends = models.DateTimeField()
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    max_presentations = models.PositiveSmallIntegerField()
    max_attendees = models.PositiveIntegerField()

    location = models.ForeignKey(
        Location,
        related_name="conferences",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_conference", kwargs={"id": self.pk})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("starts", "name")
