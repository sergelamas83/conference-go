# presentations/api_views.py
from django.http import JsonResponse
from .models import Presentation
from common.json import ModelEncoder

def api_show_presentation(id):
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation,
        encoder=ModelEncoder,
        safe=False,
    )

def api_list_presentations():
    response = []
    presentations = Presentation.objects.all()
    for presentation in presentations:
        response.append(
            {
                "title": presentation.title,
                "status": presentation.status.name,
                "href": presentation.get_api_url(),
            }
        )
    return JsonResponse({"presentations": response})
